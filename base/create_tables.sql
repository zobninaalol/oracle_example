CREATE TABLE furniture(
  article VARCHAR2(15) PRIMARY KEY,
  name VARCHAR2(50),
  partcount NUMBER,
  partnumber NUMBER
);

DROP TABLE basket;

CREATE TABLE basket(
  article VARCHAR2(15) PRIMARY KEY,
  product_name VARCHAR2(50),
  person_name VARCHAR2(50),
  itemcount NUMBER,
  dt DATE
);

CREATE TABLE phones(
  phonenumber VARCHAR2(25) PRIMARY KEY,
  firstname VARCHAR2(50),
  lastname VARCHAR2(50)
);

ALTER TABLE phones ADD(birthdate DATE);
ALTER TABLE phones ADD(childcount NUMBER);
ALTER TABLE phones ADD(sex VARCHAR2(10));