SELECT * 
FROM auto a1
WHERE mark = ANY (
    SELECT mark
    FROM auto a2
    WHERE a1.regnum <> a2.regnum
);

SELECT *
FROM city
WHERE peoples > ALL (
    SELECT peoples
    FROM city 
    WHERE citycode IN (5, 6, 7)
);