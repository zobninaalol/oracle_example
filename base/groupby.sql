SELECT mark FROM auto
GROUP BY mark;

SELECT color FROM auto
GROUP BY color;

SELECT SUBSTR(firstname, 1, 1) AS fl
FROM man
GROUP BY SUBSTR(firstname, 1, 1);

SELECT firstname, MAX(yearold) AS mx, MIN(YEAROLD) AS mn
FROM man
GROUP BY firstname;

SELECT color, COUNT(regnum)
FROM auto
GROUP BY color;

SELECT firstname, COUNT(1), AVG(yearold)
FROM man
GROUP BY firstname;

SELECT SUBSTR(cityname, 1, 1), SUM(yearold)
FROM city c INNER JOIN man m
  ON c.citycode = m.citycode
GROUP BY SUBSTR(cityname, 1, 1);

SELECT SUBSTR(cityname, 1, 2), COUNT(phonenum)
FROM city c INNER JOIN man m
  ON c.citycode = m.citycode
GROUP BY SUBSTR(cityname, 1, 2);
