SELECT firstname, lastname, SQRT(yearold)
FROM man;

SELECT firstname, lastname, yearold * COS(5)
FROM man;

SELECT * FROM city
WHERE MOD(peoples, 10000) = 0;

SELECT cityname, SQRT(peoples) * 10
FROM city
WHERE MOD(citycode, 5) = 0;