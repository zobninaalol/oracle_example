CREATE TABLE spare_parts(
  id NUMBER PRIMARY KEY,
  mark VARCHAR2(20),
  name VARCHAR2(20),
  itemcount NUMBER
);

ALTER TABLE spare_parts ADD(price NUMBER);

CREATE TABLE photo(
  name VARCHAR2(100) PRIMARY KEY,
  sign VARCHAR2(250),
  crdate DATE
);

CREATE TABLE subjects(
  name VARCHAR2(50) PRIMARY KEY,
  weekday VARCHAR2(20),
  startdate DATE
);

ALTER TABLE subjects ADD (teacher VARCHAR2(50));