SELECT * FROM man
WHERE firstname = 'Андрей';

UPDATE man
SET yearold = yearold + 1
WHERE firstname = 'Андрей';

COMMIT;

SELECT * FROM man
WHERE firstname = 'Андрей';

--NEXT--

SELECT * FROM man
WHERE phonenum LIKE '%915%';

UPDATE man
SET yearold = yearold + 2
WHERE phonenum LIKE '%915%';

COMMIT;

SELECT * FROM man
WHERE phonenum LIKE '%915%';

--NEXT--

SELECT * FROM man
WHERE phonenum LIKE '%915%';

UPDATE man
SET firstname = '915'||firstname
WHERE phonenum LIKE '%915%';

COMMIT;

SELECT * FROM man
WHERE phonenum LIKE '%915%';

--NEXT--

SELECT * FROM man
WHERE phonenum LIKE '%3';

UPDATE man
SET firstname = 'Роман'
WHERE phonenum LIKE '%3';

COMMIT;

SELECT * FROM man
WHERE phonenum LIKE '%3';