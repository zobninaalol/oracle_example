SELECT * FROM man WHERE yearold > 30;

SELECT * FROM city WHERE cityname = 'Москва';

SELECT * FROM city WHERE peoples > 1000000;

SELECT phonenum FROM man WHERE lastname != 'Денисов';

SELECT * FROM auto WHERE color = 'СИНИЙ';

-------------Подзапрос как колонка--------------

SELECT mark, color, (
    SELECT firstname 
    FROM man m
    WHERE m.phonenum = a.phonenum
) AS firstname
FROM auto a;

SELECT mark, color, (
    SELECT firstname 
    FROM man m
    WHERE m.phonenum = a.phonenum
) AS firstname,
(
    SELECT cityname
    FROM man m1 INNER JOIN city c1
    ON m1.citycode = c1.citycode
    WHERE m1.phonenum = a.phonenum
) AS city
FROM auto a;