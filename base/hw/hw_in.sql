SELECT * FROM auto
WHERE regnum IN (111114, 111115, 111116);

SELECT * FROM man
WHERE firstname IN ('Андрей', 'Максим', 'Алиса');

SELECT * FROM city
WHERE cityname IN ('Москва', 'Владимир', 'Казань');

SELECT * FROM city
WHERE citycode IN (3, 5, 7);

SELECT * 
FROM auto
WHERE phonenum IN (
    SELECT phonenum FROM man 
    WHERE SUBSTR (firstname, 1, 1) = 'А'
    );
  