SELECT * FROM man m, city c WHERE c.citycode = m.citycode;

SELECT c.cityname, c.peoples, m.firstname, m.lastname 
FROM city c, man m
WHERE c.citycode = m.citycode;

SELECT c.cityname, c.peoples, m.firstname, m.lastname 
FROM city c INNER JOIN man m
ON c.citycode = m.citycode;

SELECT * 
FROM man m LEFT JOIN auto a
ON m.phonenum = a.phonenum;

SELECT *
FROM man m RIGHT JOIN city c
ON m.citycode = c.citycode;

SELECT c1.cityname, c1.citycode, c2.cityname, c2.citycode
FROM city c1 LEFT JOIN city c2
ON c1.citycode = c2.citycode + 1;

SELECT m1.firstname, m1.lastname, m1.yearold,
  m2.firstname, m2.lastname, m2.yearold
FROM man m1 LEFT JOIN man m2
  ON m2.yearold = m1.yearold + 3
WHERE m1.yearold > 25;

SELECT m1.firstname, m1.lastname, m1.yearold,
  m2.firstname, m2.lastname, m2.yearold
FROM man m1 LEFT JOIN man m2
  ON m2.yearold > m1.yearold
WHERE m1.citycode = m2.citycode;