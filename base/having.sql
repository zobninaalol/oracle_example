SELECT color
FROM auto 
GROUP BY color
  HAVING color = 'КРАСНЫЙ' OR color = 'СИНИЙ';
  
SELECT mark, COUNT(*)
FROM auto
GROUP BY mark
  HAVING COUNT(*) > 1;
  
SELECT SUBSTR(firstname, 1, 1), AVG(yearold)
FROM man
GROUP BY SUBSTR(firstname, 1, 1)
  HAVING AVG(yearold) > 29;
  
SELECT color, COUNT(*)
FROM auto
GROUP BY color
ORDER BY color;

SELECT color, COUNT(*)
FROM auto
GROUP BY color
ORDER BY  COUNT(*) DESC;