SELECT * FROM city;

SELECT city.* FROM city;

SELECT cityname FROM city;

SELECT cityname FROM city WHERE peoples = 300000;

SELECT city.* FROM city WHERE peoples > 300000;

SELECT citycode, cityname FROM city WHERE citycode = 2;

SELECT firstname, lastname FROM man;

SELECT * FROM man WHERE yearold > 27;

SELECT firstname, lastname FROM man WHERE yearold < 55;

SELECT * FROM auto WHERE color = 'ЗЕЛЕНЫЙ';

SELECT * FROM man WHERE firstname = 'Миша';

SELECT * FROM man WHERE firstname != 'Олег';

-------------Подзапрос как колонка--------------

SELECT cityname, citycode, (
    SELECT cityname FROM city c2
    WHERE c1.citycode + 1 = c2.citycode) AS nt
FROM city c1;

SELECT firstname, lastname, (
    SELECT cityname FROM city c
    WHERE c.citycode = m.citycode) AS cityname
FROM man m;

SELECT DISTINCT mark, (
    SELECT COUNT(1) FROM man m
    WHERE phonenum IN (
        SELECT phonenum FROM auto a1
        WHERE a1.mark = a.mark
    )
) AS auto_count
FROM auto a;