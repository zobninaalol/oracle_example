SELECT * 
FROM city c
WHERE NOT EXISTS (
    SELECT 1
    FROM man m
    WHERE c.citycode = m.citycode
    );
    
SELECT firstname, lastname
FROM man m
WHERE EXISTS (
    SELECT 1 FROM city c
    WHERE c.citycode = m.citycode 
        AND c.peoples > 1000000
    );