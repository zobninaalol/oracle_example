SELECT phonenum 
FROM man
UNION
SELECT phonenum 
FROM auto;

SELECT phonenum 
FROM man
UNION ALL
SELECT phonenum 
FROM auto;

SELECT phonenum 
FROM man
MINUS
SELECT phonenum 
FROM auto;

SELECT phonenum 
FROM man
INTERSECT
SELECT phonenum 
FROM auto;