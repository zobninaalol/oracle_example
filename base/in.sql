SELECT firstname, lastname, yearold 
FROM man
WHERE yearold IN (22, 31, 34, 27);

SELECT * FROM auto
WHERE mark IN ('VOLVO', 'BMW', 'AUDI');

SELECT * FROM auto 
WHERE color IN ('СИНИЙ', 'КРАСНЫЙ', 'ЗЕЛЕНЫЙ');

SELECT * FROM city
WHERE peoples > 100000 AND citycode IN (3, 5, 7);

SELECT * 
FROM auto
WHERE phonenum IN
    (SELECT phonenum FROM man 
    WHERE yearold > 35);
    
SELECT * FROM city
WHERE citycode IN (
    SELECT citycode FROM man
    );
    
SELECT * FROM man
WHERE citycode IN (
    SELECT citycode FROM city
    WHERE peoples > 700000
    );