CREATE TABLE t1 (
    tid NUMBER,
    tname VARCHAR2(20),
    tprop VARCHAR2(20)
);

INSERT INTO t1 VALUES (1, 'круг','красный');
INSERT INTO t1 VALUES (2, 'квадрат','зеленый');
INSERT INTO t1 VALUES (3, 'ромб','синий');
INSERT INTO t1 VALUES (4, 'круг','зеленый');
INSERT INTO t1 VALUES (5, 'квадрат','красный');
INSERT INTO t1 VALUES (6, 'ромб','зеленый');
INSERT INTO t1 VALUES (7, 'круг','синий');
INSERT INTO t1 VALUES (8, 'квадрат','синий');
INSERT INTO t1 VALUES (9, 'ромб','красный');

SELECT * FROM t1;

SELECT tname, tprop, COUNT(tprop) COUNT 
FROM t1 
GROUP BY tname, tprop;

SELECT * 
FROM (
    SELECT tname, tprop
    FROM t1 
) PIVOT (
    COUNT(tprop) 
    FOR tprop 
    IN ('красный', 'синий', 'зеленый')
);