CREATE TABLE aobj1 AS
    SELECT * FROM all_objects WHERE ROWNUM < 21;
    
SELECT * FROM aobj1;
    
-- Не за полнять данными --
CREATE TABLE aobj2 AS
    SELECT object_name, object_type FROM all_objects WHERE 2=1;
    
INSERT INTO aobj2
    SELECT object_name, object_type 
    FROM all_objects 
    WHERE ROWNUM < 21;

-- С указанием столбцов --
INSERT INTO aobj2 (object_name, object_type)
    SELECT object_name, object_type 
    FROM aobj1
    WHERE ROWNUM < 21;