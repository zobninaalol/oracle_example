CREATE TABLE regtest AS 
    SELECT 'Зайцев,01111998,Киев' dt FROM DUAL 
    UNION
    SELECT 'Иванов,01011982,Москва' dt FROM DUAL
    UNION
    SELECT 'Петров,01011988,Воронеж' dt FROM DUAL;
    
SELECT * FROM regtest;

INSERT INTO regtest VALUES('Сидоров,010297,Саратов');

--------- REGEXP_LIKE ----------
SELECT * FROM regtest WHERE REGEXP_LIKE(dt, '[0-9]{8}');

--------- REGEXP_REPLACE ----------
SELECT REGEXP_REPLACE(dt, ',[0-9]{6},', ',WRONG VALUE,') FROM regtest;

--------- REGEXP_SUBSTR ----------
SELECT REGEXP_REPLACE(dt, ',[0-9]{6},', ',WRONG VALUE,') FROM regtest;

--------- REGEXP_INSTR ----------
SELECT REGEXP_INSTR(dt, '[0-9]{8}') ind FROM regtest;

--------- REGEXP_COUNT ----------
SELECT REGEXP_INSTR(dt, '[0-9]{8}') ind FROM regtest;