-- создание таблиц из select --
CREATE TABLE tsobj1 AS
    SELECT object_name, owner
    FROM all_objects
    WHERE object_type IN ('INDEX', 'TABLE')
        AND ROWNUM < 80;
        
CREATE TABLE tsobj2 AS
    SELECT object_name, owner
    FROM all_objects
    WHERE object_type IN ('TABLE')
        AND ROWNUM < 80;
        
SELECT * FROM tsobj1;

SELECT * FROM tsobj2;

-- Уберет все дубли --
SELECT * FROM tsobj1
UNION
SELECT * FROM tsobj2;

-- Будут дубли--
SELECT * FROM tsobj1
UNION ALL
SELECT * FROM tsobj2;

-- Те что есть в tsobj, но нет в tsobj2 --
SELECT * FROM tsobj1
MINUS
SELECT * FROM tsobj2;

-- Те что есть и в tsobj, и в tsobj2 --
SELECT * FROM tsobj1
INTERSECT
SELECT * FROM tsobj2;